import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import './assest/reset.css'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

import AdminIndexComponent from './component/admin/index.vue'
import IndexComponent from './component/index.vue'
import ChapterComponent from './component/chapter.vue'
import AdminStatusComponent from './component/admin/status.vue'
import AdminConfigureComponent from './component/admin/configure.vue'
import NovelComponent from './component/novel.vue'
import AdminNovelsComponent from './component/admin/novels.vue'
import AdminSearchComponent from './component/admin/search.vue'

Vue.use(VueResource)
Vue.use(VueRouter)
Vue.use(ElementUI)

const routes = [{
    path:'/admin', component: AdminIndexComponent,
    children: [
         {
            path: 'configure',
            component: AdminConfigureComponent
        }, {
            path: "novels",
            component: AdminNovelsComponent
        }, {
            path: "search",
            component: AdminSearchComponent
        },{
            path: '*',
            component: AdminStatusComponent
        },
    ]
},{
    path:'/',
    component: IndexComponent

},{
    path:'/novel/:id',
    component: NovelComponent

},{
    path:'/chapter/:id',
    component:ChapterComponent,
}]

const router = new VueRouter({
    routes // （缩写）相当于 routes: routes
})

new Vue({
    router: router
}).$mount('#app')
